import math
import warnings

import matplotlib.pyplot as plt
import numpy as np
import pyransac3d
import skg
from ellipse import LsqEllipse
from matplotlib.patches import Ellipse
from mayavi import mlab
from skspatial.objects import Plane

warnings.filterwarnings("ignore", category=RuntimeWarning) 


def compute_diameter(pointcloud):
    """Find the best fitting plane for the points"""
    # mlab.points3d(pointcloud[:,0], pointcloud[:,1], pointcloud[:,2], color=(1,1,0), mode="point")
    # mlab.show()

    plane = pyransac3d.Plane()
    eq, inliers = plane.fit(pointcloud)
    # p = pointcloud[inliers]
    # npo = pointcloud[~inliers]
    # mlab.points3d(p[:,0], p[:,1], p[:,2], color=(1,0,0), mode="point")
    # mlab.points3d(npo[:,0], npo[:,1], npo[:,2], color=(0,0,1), mode="point")
    # mlab.show()

    try:
        plane = Plane(point=[0, 0, 0], normal=eq[:3])
    except ValueError:
        return math.inf

    """ Project all points on the best fitting plane """
    projected_points = []
    for point in pointcloud:
        point_projected = plane.project_point(point)
        projected_points.append(point_projected)

    projected_points = np.array(projected_points)

    """ Substract the center to all points so the center of the points is the origin """
    projected_points[:, 0] = projected_points[:, 0] - np.mean(projected_points[:, 0])
    projected_points[:, 1] = projected_points[:, 1] - np.mean(projected_points[:, 1])
    projected_points[:, 2] = projected_points[:, 2] - np.mean(projected_points[:, 2])

    """ Translate the projected points into a 2D coordinate system """
    n = plane.normal
    n = n / np.linalg.norm(n)

    e1 = np.cross(n, [0, 0, 1])
    e1 = e1 / np.linalg.norm(e1)

    e2 = np.cross(n, e1)
    matrix = np.array([e2, e1])
    final_points = []

    for point in projected_points:
        final_points.append(matrix.dot(point))

    final_points = np.array(final_points)

    # x = []
    # y = []

    # for i in np.arange(-0.25, 0.25, 0.001):
    #     for j in np.arange(-0.25, 0.25, 0.001):
    #         x.append(i)
    #         y.append(j)

    # z = np.zeros(len(x))
    # plane = np.array([x, y, z])

    # mlab.points3d(
    #     projected_points[:, 0],
    #     projected_points[:, 1],
    #     projected_points[:, 2],
    #     mode="point",
    #     color=(1, 1, 0),
    # )
    # mlab.show()
    # mlab.points3d(plane[0], plane[1], plane[2], mode="point", color=(1, 0, 0))
    # mlab.points3d(
    #     final_points[:, 0],
    #     final_points[:, 1],
    #     np.zeros(final_points.shape[0]),
    #     mode="point",
    # )
    # mlab.show()

    """ Find best fitting ellipse """
    inner_circle=[]
    #Fit a circle two times to remove most of the center points
    for i in range(2):
        r, centerR = skg.nsphere_fit(final_points)
        circle = []
        for point in final_points:
            if np.linalg.norm(centerR - point) >= r:
                circle.append(point)
            else:
                inner_circle.append(point)

        circle = np.array(circle)
        final_points = circle

    inner_circle = np.array(inner_circle)

    #Fit an ellipse
    try:
        reg = LsqEllipse().fit(circle)
        centerR, width, height, phi = reg.as_parameters()
    except (ValueError, IndexError, np.linalg.LinAlgError) as e:
        return math.inf

    # try:
    #     fig = plt.figure(figsize=(6, 6))
    #     ax = plt.subplot()
    #     ax.axis("equal")
    #     ax.plot(circle[:, 0], circle[:, 1], "ro", zorder=1)
    #     ax.plot(inner_circle[:, 0], inner_circle[:, 1], "bo", zorder=1)

    #     t = np.linspace(0, 2 * np.pi, 1000, endpoint=True)
    #     plt.plot(r * np.cos(t) + centerR[0], r * np.sin(t) + centerR[1], color="green")

    #     ellipse = Ellipse(
    #         xy=centerR,
    #         width=width * 2,
    #         height=height * 2,
    #         angle=np.rad2deg(phi),
    #         edgecolor="b",
    #         fc="None",
    #         lw=2,
    #         label="Fit",
    #         zorder=2,
    #     )
    #     ax.add_patch(ellipse)

    #     x = np.arange(0,width,0.0001)
    #     ax.plot(x, np.zeros(x.shape)*0.01)

    #     plt.show()
    # except ValueError:
    #     pass

    if width > height:
        return width * 2
    else:
        return height * 2
