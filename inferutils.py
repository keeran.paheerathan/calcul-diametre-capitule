import numpy as np
import torch
from mayavi import mlab

from utils import normalize_pointcloud, to_categorical


def infer(classifier, pcd, num_sample_points=4096, num_classes=16):
    seg_classes = {"Sunflower": [0, 1, 2]} #Sunflower parts 0: Flowerhead, 1:Leaf, 2:Top of stem

    seg_label_to_cat = {}
    for cat in seg_classes.keys():
        for label in seg_classes[cat]:
            seg_label_to_cat[label] = cat

    #We keep a copy of the pointcloud,
    #because we lose the pointcloud size when normalizing the pointcloud
    unmodified_pcd = pcd.copy() 
    
    pcd = normalize_pointcloud(pcd)

    #Downsample pointcloud
    choice = np.random.choice(len(pcd), num_sample_points, replace=True)
    pcd = pcd[choice, :]
    unmodified_pcd = unmodified_pcd[choice, :]

    pcd_infer = np.zeros((num_sample_points, 6))
    pcd_infer[:, :3] = pcd #(N,6) (N,(pointcoordinates, normals)), we have no normals, so normals = (0,0,0)
    pcd_infer = torch.from_numpy(np.expand_dims(pcd_infer, axis=0)) #Create batch of size one
    cls = torch.tensor([[0]])
    pcd_infer = pcd_infer.float()
    cls = cls.long()

    pcd_infer = pcd_infer.transpose(2, 1)
    seg_pred, _ = classifier(pcd_infer, to_categorical(cls, num_classes))
    cur_pred_val = seg_pred.cpu().data.numpy()
    cur_pred_val_logits = cur_pred_val
    cur_pred_val = np.zeros((1, num_sample_points)).astype(np.int32)

    for i in range(1):
        cat = seg_label_to_cat[0]
        logits = cur_pred_val_logits[i, :, :]
        cur_pred_val[i, :] = (
            np.argmax(logits[:, seg_classes[cat]], 1) + seg_classes[cat][0]
        )

    return unmodified_pcd, cur_pred_val[0]


def visualize(flowerhead, top_of_stem, leaf):
    COLORS = [
        (1, 1, 0),
        (0, 0.4, 0.2),
        (0, 1, 0),
    ]

    if len(flowerhead) > 0:
        flowerhead = np.array(flowerhead)
        mlab.points3d(
            flowerhead[:, 0],
            flowerhead[:, 1],
            flowerhead[:, 2],
            mode="point",
            color=COLORS[0],
        )

    if len(top_of_stem) > 0:
        top_of_stem = np.array(top_of_stem)
        mlab.points3d(
            top_of_stem[:, 0],
            top_of_stem[:, 1],
            top_of_stem[:, 2],
            mode="point",
            color=COLORS[1],
        )
    if len(leaf) > 0:
        leaf = np.array(leaf)
        mlab.points3d(
            leaf[:, 0],
            leaf[:, 1],
            leaf[:, 2],
            mode="point",
            color=COLORS[2],
        )
