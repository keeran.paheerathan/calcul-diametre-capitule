import argparse
import os
import time
from glob import glob

import numpy as np
import pylas
import torch
from mayavi import mlab
from tqdm import tqdm

from diameter import compute_diameter
from inferutils import infer, visualize
from models import pointnet2_part_seg_msg
from preprocessing import denoise, remove_ground, remove_lidar_trajectory
from utils import create_clusters, reject_outliers

start_time = time.time()

NUM_CLASSES = 3

""" LOADING ARGUMENT PARSER """
parser = argparse.ArgumentParser()
parser.add_argument("--las_path", required=True, dest="las_path")
parser.add_argument("--output_path", default="", dest="output_path")
# Boolean arguments
parser.add_argument("--visualization", action="store_true")
parser.add_argument("--no-visualization", dest="visualization", action="store_false")
parser.set_defaults(visualization=False)

args = parser.parse_args()

""" LOADING THE POINTCLOUDS """
print("Loading pointclouds...")
pointcloud_locations = glob(os.path.join(args.las_path) + "\*point_cloud.las")
pointclouds = []

for pointcloud_location in pointcloud_locations:
    with pylas.open(pointcloud_location) as f:
        point_cloud = f.read()
        pointclouds.append(np.array([point_cloud.x, point_cloud.y, point_cloud.z]))

""" PREPROCESSING AND FUSION OF THE POINTCLOUDS """
print("Preprocessing...")
pointclouds = remove_lidar_trajectory(pointclouds)
# pointclouds = refine(pointclouds) #Removes the border rows of the plot because there is not enough point to work with
pointcloud = np.concatenate((pointclouds[0].T, pointclouds[1].T, pointclouds[2].T))

pointcloud = remove_ground(pointcloud)
pointcloud = denoise(pointcloud)

""" CREATE CLUSTERS FROM THE POINTCLOUD """
print("Creating clusters...")
cluster_to_infer, other_cluster = create_clusters(pointcloud)

if args.visualization == True:
    for cluster in other_cluster:
        mlab.points3d(
            cluster[:, 0], cluster[:, 1], cluster[:, 2], mode="point", color=(1, 0, 0)
        )

""" INFERENCE """
print("Inference...")
## Instantiate the model
classifier = pointnet2_part_seg_msg.get_model(NUM_CLASSES, normal_channel=True)
checkpoint = torch.load(
    "log\\part_seg\\pointnet2_part_seg_msg_global\\checkpoints\\best_model.pth"
)
classifier.load_state_dict(checkpoint["model_state_dict"])
classifier = classifier.eval()

flowerhead_diameters = []

for pcd in tqdm(cluster_to_infer):
    unmodified_pcd, predicted_labels = infer(classifier, pcd)

    flowerhead = unmodified_pcd[np.where(predicted_labels == 0)]
    leaf = unmodified_pcd[np.where(predicted_labels == 1)]
    top_of_stem = unmodified_pcd[np.where(predicted_labels == 2)]

    # If there is enough points in the flowerhead pointcloud we will compute the diameter.
    if len(flowerhead) > 12:
        try:
            diameter = compute_diameter(flowerhead)
            flowerhead_diameters.append(diameter.real)
        except (RuntimeError, TypeError):
            pass

    if args.visualization == True:
        visualize(flowerhead, top_of_stem, leaf)


flowerhead_diameters = reject_outliers(np.array(flowerhead_diameters), m=2.5)

print("Found {} diameters.".format(len(flowerhead_diameters)))

if not os.path.exists(args.output_path):
    os.makedirs(args.output_path)

np.savetxt(
    os.path.join(args.output_path, "flowerhead_diameters.csv"),
    flowerhead_diameters,
    delimiter=",",
)

end_time = time.time() - start_time
print("--- %s seconds ---" % end_time)

if args.visualization == True:
    mlab.show()
