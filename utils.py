import math

import numpy as np
import open3d as o3d
import torch
from tqdm import tqdm


def get_clusters(
    pointcloud: np.array, min_points: int = 512, eps: float = 0.04
) -> list:
    """Create clusters from the pointcloud with DBSCAN

    Args:
        pointcloud (np.array): Pointcloud (N,3)
        min_points (int, optional): Minimum points per cluster. Defaults to 512.
        eps (float, optional): DBSCAN eps parameter. Defaults to 0.04.

    Returns:
        list: List of np.array
    """
    pointcloud_o3d = o3d.geometry.PointCloud()
    pointcloud_o3d.points = o3d.utility.Vector3dVector(pointcloud)

    labels = np.array(pointcloud_o3d.cluster_dbscan(eps=eps, min_points=min_points))

    unique_labels = set(labels)
    try:
        unique_labels.remove(-1)
    except KeyError:
        pass

    clusters = []

    for label in unique_labels:
        clusters.append(pointcloud[np.where(labels == label)])

    return clusters


def create_clusters(pointcloud: np.array) -> list:
    """Transforms a plot pointcloud into clusters

    Args:
        pointcloud (np.array): Pointcloud (N,3)

    Returns:
        list: List with the clusters to keep and the other clusters
    """
    cluster_to_infer = []
    other_cluster = []

    clusters = get_clusters(pointcloud)

    # Make more clusters from clusters that are too big
    cluster_8192 = [cluster for cluster in clusters if cluster.shape[0] >= 8192]
    clusters = [cluster for cluster in clusters if cluster.shape[0] < 8192]

    while len(cluster_8192) > 0:
        temp_clusters = []
        for i, cluster in enumerate(cluster_8192):
            temp = get_clusters(cluster, 1024)
            if len(temp) > 1:
                temp_clusters += temp
            else:
                if cluster.shape[0] > 16384:
                    temp_clusters += get_clusters(cluster, 512, eps=0.01)
                else:
                    clusters.append(cluster)
                    cluster_8192.pop(i)

        for i, cluster in enumerate(temp_clusters):
            if cluster.shape[0] < 8192:
                clusters.append(cluster)
                temp_clusters.pop(i)

        cluster_8192 = temp_clusters

    # Find max and min height of the plot
    max_height = -math.inf
    min_height = math.inf
    for cluster in clusters:
        if np.max(cluster[:, 2]) > max_height:
            max_height = np.max(cluster[:, 2])
        if np.min(cluster[:, 2]) < min_height:
            min_height = np.min(cluster[:, 2])

    for cluster in tqdm(clusters):
        if cluster.shape[0] >= 512 and np.mean(cluster[:, 2]) > (
            (max_height + min_height) / 2
        ):
            cluster_to_infer.append(cluster)
        else:
            other_cluster.append(cluster)

    return cluster_to_infer, other_cluster


def normalize_pointcloud(pointcloud: np.array) -> np.array:
    """Moves the pointcloud center to the origin and normalize its size (like normalizing a vector)

    Args:
        (np.array (N,3)): pointcloud 

    Returns:
        np.array (N,3): Normalized pointcloud
    """
    centroid = np.mean(pointcloud, axis=0)
    pointcloud = pointcloud - centroid
    m = np.max(np.sqrt(np.sum(pointcloud**2, axis=1)))
    pointcloud = pointcloud / m
    return pointcloud


def to_categorical(y, num_classes):
    """1-hot encodes a tensor"""
    new_y = torch.eye(num_classes)[
        y.cpu().data.numpy(),
    ]
    if y.is_cuda:
        return new_y.cuda()
    return new_y


def reject_outliers(data, m=2.0):
    d = np.abs(data - np.median(data))
    mdev = np.median(d)
    s = d / mdev if mdev else 0.0
    return data[s < m]
