# Flowerhead Diameter

This program measures the diameters of sunflowers flowerheads from pointclouds of a sunflower plot acquired by the Phenomobile.  
It uses a Pytorch implementation of Pointnet++ (https://github.com/yanx27/Pointnet_Pointnet2_pytorch), a Deep Learning model, to do part segmentation on the sunflower plot and detect the flowerheads.

![](https://forgemia.inra.fr/keeran.paheerathan/calcul-diametre-capitule/-/raw/main/images/inference.png)


## Installation

Install the package that are in the requirements.txt file:  
`pip install -r requirements.txt` 

Install the CSF package from: https://github.com/jianboqi/CSF

## Get started

Command line argument are the following:

| Name | header |
| ------ | ------ |
| --las_path | The path of the folder containing the three pointcloud files in *.las format |
| --output | The path of the folder where the diameters of the flowerheads will be saved |
| --visualization | This parameter is optional. If this parameter is present, a visualization of the inference of the plot will be shown. |

Exemple commands:

`python inferplot.py --las_path "C:\Users\kpaheeratha\Documents\Code\Module Extraction\output" --output output --visualization`  
`python inferplot.py --las_path "C:\Users\kpaheeratha\Documents\Code\Module Extraction\output" --output other_output`
