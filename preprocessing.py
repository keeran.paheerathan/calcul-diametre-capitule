import numpy as np
import open3d as o3d

import CSF


def remove_lidar_trajectory(pointclouds: list, threshold=0.5) -> list:
    """Removes the points located in the lidars trajectories.

    Args:
        pointclouds (list): List of the three pointclouds np.array (3, N)

    Returns:
        list: List of the three pointclouds np.array (3, N)
    """

    # Find the top points of the pointcloud to remove them
    for i in range(len(pointclouds)):
        pointcloud = pointclouds[i].T
        max_z = np.max(pointcloud[:, 2])
        pointcloud = pointcloud[np.where(pointcloud[:, 2] < max_z - threshold)]
        pointclouds[i] = pointcloud.T

    return pointclouds


def refine(pointclouds: list) -> list:
    """Remove the border rows of the because there is not enough points to work with.

    Args:
        pointclouds (list): List of the three pointclouds np.array (3, N)

    Returns:
        list: List of the three pointclouds np.array (3, N)
    """
    min_y = min(pointclouds[2][1])
    max_y = max(pointclouds[0][1])

    min_x = min(pointclouds[2][0])
    max_x = max(pointclouds[0][0])

    for i in range(len(pointclouds)):
        pcd = pointclouds[i].T
        pcd = pcd[np.where(np.logical_and(pcd[:, 0] > min_x, pcd[:, 0] < max_x))]
        pcd = pcd[np.where(np.logical_and(pcd[:, 1] > min_y, pcd[:, 1] < max_y))]
        pointclouds[i] = pcd.T

    return pointclouds


def remove_ground(pointcloud: np.array) -> np.array:
    """Removes the ground of the plot

    Args:
        pointcloud (np.array): Pointcloud (N,3)

    Returns:
        np.array: Pointcloud (N,3)
    """
    csf = CSF.CSF()

    # prameter settings
    csf.params.bSloopSmooth = False
    csf.params.cloth_resolution = 0.1
    csf.params.rigidness = 3

    csf.params.time_step = 0.4
    csf.params.class_threshold = 0.1
    # more details about parameter: http://ramm.bnu.edu.cn/projects/CSF/download/

    csf.setPointCloud(pointcloud)
    ground = (
        CSF.VecInt()
    )  # a list to indicate the index of ground points after calculation
    non_ground = (
        CSF.VecInt()
    )  # a list to indicate the index of non-ground points after calculation
    csf.do_filtering(ground, non_ground)  # do actual filtering.

    return pointcloud[non_ground]


def denoise(pointcloud: np.array) -> np.array:
    """ Denoises the pointcloud.

    Args:
        pointcloud (np.array): Pointcloud (N,3)

    Returns:
        np.array: Pointcloud (N,3)
    """
    pcd_o3d = o3d.geometry.PointCloud()
    pcd_o3d.points = o3d.utility.Vector3dVector(pointcloud)

    labels = np.array(
        pcd_o3d.cluster_dbscan(eps=0.008545, min_points=15, print_progress=False)
    )

    x_to_delete = list(np.where(labels == -1)[0])

    pcd_o3d = pcd_o3d.select_by_index(x_to_delete, invert=True)
    return np.asarray(pcd_o3d.points)
